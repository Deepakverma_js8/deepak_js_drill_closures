const limitFunction = require('../limitFunctionCallCount');

const Count = limitFunction( () => {
    console.log("Hello world");
},5);

if(Count !== null){
    for(let i = 0; i < 5 ; i++){
        Count();
    }
}
