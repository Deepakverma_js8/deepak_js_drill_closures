const cacheFunction = require('../cacheFunction') ;

function cb (args) {
    return args * 2 ; 
}

const cache = cacheFunction(cb);

for(let i = 0 ; i < 5 ; i++){
    console.log(cache(i)) ;
}
